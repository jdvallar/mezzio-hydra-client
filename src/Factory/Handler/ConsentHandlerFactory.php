<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Handler;


use Vallarj\Mezzio\HydraClient\Exception\Exception;
use Vallarj\Mezzio\HydraClient\Handler\ConsentHandler;
use Ory\Hydra\Client\Api\AdminApi;
use Psr\Container\ContainerInterface;

class ConsentHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $providers = $config['hydra']['providers'] ?? [];
        $scopeProvider = $providers['scope'] ?? null;
        $userClaimsProvider = $providers['user_claims'] ?? null;
        $firstPartyProvider = $providers['first_party_verifier'] ?? null;

        if (!$scopeProvider) {
            throw new Exception("Missing configuration key: [hydra][providers][scope]");
        }

        if (!$firstPartyProvider) {
            throw new Exception("Missing configuration key: [hydra][providers][first_party_verifier]");
        }

        if (!$userClaimsProvider) {
            throw new Exception("Missing configuration key: [hydra][providers][user_claims]");
        }

        return new ConsentHandler(
            $container->get(AdminApi::class),
            $container->get($scopeProvider),
            $container->get($firstPartyProvider),
            $container->get($userClaimsProvider)
        );
    }
}
