<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Handler;


use Vallarj\Mezzio\HydraClient\Handler\LogoutHandler;
use Vallarj\OAuth2\Client\Provider\Hydra;
use Ory\Hydra\Client\Api\AdminApi;
use Psr\Container\ContainerInterface;

class LogoutHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new LogoutHandler(
            $container->get(AdminApi::class),
            $container->get(Hydra::class)
        );
    }
}
