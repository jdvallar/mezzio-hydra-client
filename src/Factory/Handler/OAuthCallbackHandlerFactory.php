<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Handler;


use Vallarj\Mezzio\HydraClient\Handler\OAuthCallbackHandler;
use Vallarj\OAuth2\Client\Provider\Hydra;
use Psr\Container\ContainerInterface;

class OAuthCallbackHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new OAuthCallbackHandler($container->get(Hydra::class));
    }
}
