<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Handler;


use Vallarj\Mezzio\HydraClient\Exception\Exception;
use Vallarj\Mezzio\HydraClient\Handler\LoginHandler;
use Ory\Hydra\Client\Api\AdminApi;
use Psr\Container\ContainerInterface;

class LoginHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $providers = $config['hydra']['providers'] ?? [];
        $authenticator = $providers['authenticator'] ?? null;

        if (!$authenticator) {
            throw new Exception("Missing configuration key: [hydra][providers][authenticator]");
        }

        return new LoginHandler(
            $container->get(AdminApi::class),
            $container->get($authenticator)
        );
    }
}
