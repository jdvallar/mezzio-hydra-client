<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Handler;


use Vallarj\Mezzio\HydraClient\Exception\Exception;
use Vallarj\Mezzio\HydraClient\Handler\SessionHandler;
use Vallarj\OAuth2\Client\Provider\Hydra;
use Ory\Hydra\Client\Api\AdminApi;
use Psr\Container\ContainerInterface;

class SessionHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $providers = $config['hydra']['providers'] ?? [];
        $scopeProvider = $providers['scope'] ?? null;

        if (!$scopeProvider) {
            throw new Exception("Missing configuration key: [hydra][providers][scope]");
        }

        return new SessionHandler(
            $container->get(Hydra::class),
            $container->get(AdminApi::class),
            $container->get($scopeProvider)
        );
    }
}
