<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Hydra;


use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\Configuration;
use Psr\Container\ContainerInterface;

class AdminApiFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        if (!isset($config["hydra"]["admin"]["host"])) {
            throw new \Exception("Configuration key [hydra][admin][host] is missing.");
        }

        $hydraConfig = new Configuration();
        $hydraConfig->setHost($config["hydra"]["admin"]["host"]);

        return new AdminApi(null, $hydraConfig);
    }
}
