<?php


namespace Vallarj\Mezzio\HydraClient\Factory\Hydra;


use Vallarj\OAuth2\Client\Provider\Hydra;
use Vallarj\OAuth2\Client\Provider\OIDCDiscovery;
use Psr\Container\ContainerInterface;

class HydraClientProviderFactory
{
    private const KEY_CLIENT_ID = "client_id";
    private const KEY_SECRET = "secret";
    private const KEY_REDIRECT_URI = "redirect_uri";
    private const KEY_SERVER_URL = "server_url";
    private const KEY_OIDC_CACHE_DIR = 'oidc_cache_dir';

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $clientConfig = $config["hydra"]["client"] ?? [];

        if (!isset($clientConfig[self::KEY_CLIENT_ID])) {
            throw new \Exception("Configuration key [hydra][client][client_id] is missing.");
        }

        if (!isset($clientConfig[self::KEY_SECRET])) {
            throw new \Exception("Configuration key [hydra][admin][secret] is missing.");
        }

        if (!isset($clientConfig[self::KEY_REDIRECT_URI])) {
            throw new \Exception("Configuration key [hydra][admin][redirect_uri] is missing.");
        }

        if (!isset($clientConfig[self::KEY_SERVER_URL])) {
            throw new \Exception("Configuration key [hydra][admin][server_url] is missing.");
        }

        if (!isset($clientConfig[self::KEY_OIDC_CACHE_DIR])) {
            throw new \Exception("Configuration key [hydra][admin][oidc_cache_dir] is missing.");
        }

        $verifySSL = $config["hydra"]["verify-ssl"];

        return new Hydra(
            $clientConfig[self::KEY_CLIENT_ID],
            $clientConfig[self::KEY_SECRET],
            $clientConfig[self::KEY_REDIRECT_URI],
            [],
            $verifySSL,
            new OIDCDiscovery(
                $clientConfig[self::KEY_SERVER_URL],
                $clientConfig[self::KEY_OIDC_CACHE_DIR],
                $verifySSL
            )
        );
    }
}
