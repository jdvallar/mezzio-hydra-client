<?php


namespace Vallarj\Mezzio\HydraClient;


class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'hydra' => [
                'verify-ssl' => true
            ]
        ];
    }
    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies() : array
    {
        return [
            'factories'  => [
                \Ory\Hydra\Client\Api\AdminApi::class => Factory\Hydra\AdminApiFactory::class,
                \Vallarj\OAuth2\Client\Provider\Hydra::class => Factory\Hydra\HydraClientProviderFactory::class,
                Handler\ConsentHandler::class => Factory\Handler\ConsentHandlerFactory::class,
                Handler\LoginHandler::class => Factory\Handler\LoginHandlerFactory::class,
                Handler\LogoutHandler::class => Factory\Handler\LogoutHandlerFactory::class,
                Handler\OAuthCallbackHandler::class => Factory\Handler\OAuthCallbackHandlerFactory::class,
                Handler\SessionHandler::class => Factory\Handler\SessionHandlerFactory::class
            ],
        ];
    }
}
