<?php


namespace Vallarj\Mezzio\HydraClient\Provider;


interface ScopeProvider
{
    /**
     * Returns an array of scope strings
     *
     * @return string[]
     */
    public function getScopes(): array;

    /**
     * Returns the description of a given scope
     *
     * @param string $scope
     * @return string
     */
    public function getScopeDescription(string $scope): string;
}
