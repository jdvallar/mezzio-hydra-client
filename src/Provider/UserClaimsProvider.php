<?php


namespace Vallarj\Mezzio\HydraClient\Provider;


interface UserClaimsProvider
{
    /**
     * Returns an array with key-value pair user claims
     *
     * @param string $userId
     * @return array
     */
    public function getUserClaims(string $userId): array;
}
