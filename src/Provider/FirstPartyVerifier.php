<?php


namespace Vallarj\Mezzio\HydraClient\Provider;


interface FirstPartyVerifier
{
    /**
     * Returns if given client ID is recognized as first-party
     *
     * @param string $clientId
     * @return bool
     */
    public function isFirstPartyClient(string $clientId): bool;
}
