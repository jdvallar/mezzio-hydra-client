<?php


namespace Vallarj\Mezzio\HydraClient\Provider;


interface Authenticator
{
    /**
     * Returns true if a given username-password combination is valid
     *
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function authenticate(string $username, string $password): bool;

    /**
     * Returns the equivalent user ID of a given username-password combination
     *
     * @param string $username
     * @param string $password
     * @return string
     */
    public function getUserId(string $username, string $password): string;
}
