<?php


namespace Vallarj\Mezzio\HydraClient\Handler;


use Vallarj\OAuth2\Client\Provider\Hydra;
use Laminas\Diactoros\Response\RedirectResponse;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\ApiException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LogoutHandler implements RequestHandlerInterface
{
    /** @var AdminApi */
    private $hydraAdmin;

    /** @var Hydra */
    private $hydraClientProvider;

    /**
     * LogoutHandler constructor.
     *
     * @param AdminApi $hydraAdmin
     * @param Hydra $hydraClientProvider
     */
    public function __construct(AdminApi $hydraAdmin, Hydra $hydraClientProvider)
    {
        $this->hydraAdmin = $hydraAdmin;
        $this->hydraClientProvider = $hydraClientProvider;
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $query = $request->getQueryParams();
        $challenge = $query["logout_challenge"] ?? null;

        if (!$challenge) {
            $endSessionEndpoint = $this->hydraClientProvider->getBaseEndSessionURL();
            return new RedirectResponse($endSessionEndpoint);
        }

        try {
            $response = $this->hydraAdmin->acceptLogoutRequest($challenge);

            $session = $this->getSession($request);
            $accessToken = $session->get("access_token");
            $refreshToken = $session->get("refresh_token");

            $session->clear();

            if ($accessToken && $refreshToken) {
                $this->hydraClientProvider->revokeToken($accessToken, $refreshToken);
            }

            return new RedirectResponse($response->getRedirectTo());
        } catch (ApiException $e) {
        }

        return new RedirectResponse("/");
    }

    /**
     * Returns the session container
     *
     * @param ServerRequestInterface $request
     * @return SessionInterface
     */
    public function getSession(ServerRequestInterface $request): SessionInterface
    {
        return $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
    }
}
