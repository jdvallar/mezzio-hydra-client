<?php


namespace Vallarj\Mezzio\HydraClient\Handler;


use Vallarj\Mezzio\HydraClient\Provider\Authenticator;
use Exception;
use Laminas\Diactoros\Response\JsonResponse;
use Mezzio\Csrf\CsrfGuardInterface;
use Mezzio\Csrf\CsrfMiddleware;
use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\ApiException;
use Ory\Hydra\Client\Model\AcceptLoginRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LoginHandler implements RequestHandlerInterface
{
    /** @var AdminApi */
    private $hydraAdmin;

    /** @var Authenticator */
    private $authenticator;

    /**
     * LoginHandler constructor.
     *
     * @param AdminApi $adminApi
     * @param Authenticator $authenticator
     */
    public function __construct(AdminApi $adminApi, Authenticator $authenticator)
    {
        $this->hydraAdmin = $adminApi;
        $this->authenticator = $authenticator;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $method = $request->getMethod();
        return $method == "GET" ? $this->handleGet($request) : $this->handlePost($request);
    }

    /**
     * Handle GET request
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function handleGet(ServerRequestInterface $request): ResponseInterface
    {
        $challenge = $request->getQueryParams()["login_challenge"] ?? "";

        try {
            $loginResponse = $this->hydraAdmin->getLoginRequest($challenge);

            if ($loginResponse->getSkip()) {
                $acceptLoginRequest = new AcceptLoginRequest([
                    "subject" => $loginResponse->getSubject()
                ]);

                $acceptResponse = $this->hydraAdmin->acceptLoginRequest($challenge, $acceptLoginRequest);
                $response = (object)["status_code" => 302, "redirect_to" => $acceptResponse->getRedirectTo()];
            } else {
                $guard = $this->getCSRFGuard($request);

                $response = (object)[
                    "status_code" => 400,
                    "state" => $guard->generateToken()
                ];
            }
        } catch (ApiException $e) {
            // TODO: Login challenge invalid at this point, so a 404 is more appropriate here.
            $response = json_decode($e->getResponseBody());
        }

        return new JsonResponse($response);
    }

    /**
     * Handle POST request
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function handlePost(ServerRequestInterface $request): ResponseInterface
    {
        // TODO: Handle invalid body.
        $body = json_decode($request->getBody()->getContents(), true);
        $username = $body["username"];
        $password = $body["password"];
        $challenge = $body["login_challenge"];
        $token = $body["state"] ?? "";
        $guard = $this->getCSRFGuard($request);

        // Check if valid credentials
        if (!$this->authenticator->authenticate($username, $password)
            || !$guard->validateToken($token)
        ) {
            $response = (object)[
                "status_code" => 401,
                "state" => $guard->generateToken()
            ];
        } else {
            $acceptLoginRequest = new AcceptLoginRequest([
                "subject" =>  $this->authenticator->getUserId($username, $password),
                "remember" => true,
                "rememberFor" => 0,
                "acr" => '0'
            ]);

            try {
                $acceptResponse = $this->hydraAdmin->acceptLoginRequest($challenge, $acceptLoginRequest);
                $response = (object)["status_code" => 302, "redirect_to" => $acceptResponse->getRedirectTo()];
            } catch (ApiException $e) {
                $response = json_decode($e->getResponseBody());
            }
        }

        return new JsonResponse($response);
    }

    /**
     * Returns the CSRF Guard
     *
     * @param ServerRequestInterface $request
     * @return CsrfGuardInterface
     */
    private function getCSRFGuard(ServerRequestInterface $request): CsrfGuardInterface
    {
        return $request->getAttribute(CsrfMiddleware::GUARD_ATTRIBUTE);
    }
}
