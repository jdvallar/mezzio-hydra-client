<?php


namespace Vallarj\Mezzio\HydraClient\Handler;


use Vallarj\OAuth2\Client\Provider\Hydra;
use Laminas\Diactoros\Response\RedirectResponse;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class OAuthCallbackHandler implements RequestHandlerInterface
{
    /** @var Hydra */
    private $hydraClientProvider;

    /**
     * OAuthCallbackHandler constructor.
     *
     * @param Hydra $hydraClientProvider
     */
    public function __construct(Hydra $hydraClientProvider)
    {
        $this->hydraClientProvider = $hydraClientProvider;
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $query = $request->getQueryParams();
        $session = $this->getSession($request);

        if ($session->has('user_id')) {
            return new RedirectResponse('/');
        }

        if (
            !isset($query["code"])
            || empty($query['state'])
            || !$session->has('oauth2state')
            || $query['state'] !== $session->get('oauth2state')
        ) {
            $session->clear();
            return new RedirectResponse('/');
        }

        try {
            // Try to get an access token using the authorization code grant.
            $accessToken = $this->hydraClientProvider->getAccessToken(
                'authorization_code',
                [
                    'code' => $query["code"],
                    'nonce' => $session->get('oauth2nonce')
                ]
            );

            $idToken = $accessToken->getIdToken();
            $claims = $idToken->claims();
            $session->set("access_token", $accessToken->getToken());
            $session->set("expires", $accessToken->getExpires());
            $session->set("refresh_token", $accessToken->getRefreshToken());
            $session->set("user_id", $claims->get("sub"));
            $session->set("usr", $claims->get("usr"));
        } catch (IdentityProviderException $e) {
            $session->clear();
        }

        return new RedirectResponse('/');
    }

    /**
     * Returns the session container
     *
     * @param ServerRequestInterface $request
     * @return SessionInterface
     */
    public function getSession(ServerRequestInterface $request): SessionInterface
    {
        return $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
    }
}
