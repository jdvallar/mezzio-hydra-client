<?php


namespace Vallarj\Mezzio\HydraClient\Handler;


use Vallarj\Mezzio\HydraClient\Provider\FirstPartyVerifier;
use Vallarj\Mezzio\HydraClient\Provider\ScopeProvider;
use Vallarj\Mezzio\HydraClient\Provider\UserClaimsProvider;
use Laminas\Diactoros\Response\JsonResponse;
use Mezzio\Csrf\CsrfGuardInterface;
use Mezzio\Csrf\CsrfMiddleware;
use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\ApiException;
use Ory\Hydra\Client\Model\AcceptConsentRequest;
use Ory\Hydra\Client\Model\ConsentRequest;
use Ory\Hydra\Client\Model\ConsentRequestSession;
use Ory\Hydra\Client\Model\RejectRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ConsentHandler implements RequestHandlerInterface
{
    /** @var AdminApi */
    private $hydraAdmin;

    /** @var ScopeProvider */
    private $scopeProvider;

    /** @var FirstPartyVerifier */
    private $firstPartyVerifier;

    /** @var UserClaimsProvider */
    private $userClaimsProvider;

    /**
     * ConsentHandler constructor.
     *
     * @param AdminApi $adminApi
     * @param ScopeProvider $scopeProvider
     * @param FirstPartyVerifier $firstPartyVerifier
     * @param UserClaimsProvider $userClaimsProvider
     */
    public function __construct(
        AdminApi $adminApi,
        ScopeProvider $scopeProvider,
        FirstPartyVerifier $firstPartyVerifier,
        UserClaimsProvider $userClaimsProvider
    ) {
        $this->hydraAdmin = $adminApi;
        $this->scopeProvider = $scopeProvider;
        $this->firstPartyVerifier = $firstPartyVerifier;
        $this->userClaimsProvider = $userClaimsProvider;
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $method = $request->getMethod();
        return $method == 'GET' ? $this->handleGet($request) : $this->handlePost($request);
    }

    /**
     * Handle GET request.
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function handleGet(ServerRequestInterface $request): ResponseInterface
    {
        $challenge = $request->getQueryParams()["consent_challenge"] ?? "";

        try {
            $consentResponse = $this->hydraAdmin->getConsentRequest($challenge);
            $clientId = $consentResponse->getClient()->getClientId();

            if ($consentResponse->getSkip() || $this->firstPartyVerifier->isFirstPartyClient($clientId)) {
                $userId = $consentResponse->getSubject();
                $userClaims = $this->getUserClaims($userId);

                $session = new ConsentRequestSession();
                $session->setAccessToken($userClaims);
                $session->setIdToken($userClaims);

                $acceptConsentRequest = new AcceptConsentRequest([
                    "grantScope" => $consentResponse->getRequestedScope(),
                    "grantAccessTokenAudience" => $consentResponse->getRequestedAccessTokenAudience(),
                    "session" => $session
                ]);

                $acceptResponse = $this->hydraAdmin->acceptConsentRequest($challenge, $acceptConsentRequest);
                $response = (object)["status_code" => 302, "redirect_to" => $acceptResponse->getRedirectTo()];
            } else {
                $guard = $this->getCSRFGuard($request);

                $response = (object)[
                    "status_code" => 400,
                    "requested_scope" => $this->getRequestedScopes($consentResponse),
                    "user" => $consentResponse->getSubject(),
                    "client" => $consentResponse->getClient()->getClientName(),
                    "state" => $guard->generateToken()
                ];
            }
        } catch (ApiException $e) {
            // TODO: Consent challenge invalid at this point, so a 404 is more appropriate here.
            $response = json_decode($e->getResponseBody());
        }

        return new JsonResponse($response);
    }

    /**
     * Handle POST request.
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function handlePost(ServerRequestInterface $request): ResponseInterface
    {
        // TODO: Handle invalid body and incomplete requests.
        $body = json_decode($request->getBody()->getContents(), true);
        $allow = $body["allow"];
        $challenge = $body["consent_challenge"];
        $token = $body["state"] ?? "";
        $guard = $this->getCSRFGuard($request);

        try {
            if (!$allow || !$guard->validateToken($token)) {
                $rejectConsentRequest = new RejectRequest([
                    "error" => 'access_denied',
                    "errorDescription" => 'The resource owner denied the request'
                ]);

                $rejectResponse = $this->hydraAdmin->rejectConsentRequest($challenge, $rejectConsentRequest);
                $response = (object)["status_code" => 302, "redirect_to" => $rejectResponse->getRedirectTo()];
            } else {
                $consentResponse = $this->hydraAdmin->getConsentRequest($challenge);
                $userId = $consentResponse->getSubject();
                $userClaims = $this->getUserClaims($userId);

                $session = new ConsentRequestSession();
                $session->setAccessToken($userClaims);
                $session->setIdToken($userClaims);
                $acceptConsentRequest = new AcceptConsentRequest([
                    "grantScope" => $consentResponse->getRequestedScope(),
                    "grantAccessTokenAudience" => $consentResponse->getRequestedAccessTokenAudience(),
                    "session" => $session,
                    "remember" => true,
                    "rememberFor" => 0
                ]);

                $acceptResponse = $this->hydraAdmin->acceptConsentRequest($challenge, $acceptConsentRequest);
                $response = (object)["status_code" => 302, "redirect_to" => $acceptResponse->getRedirectTo()];
            }
        } catch (ApiException $e) {
            $response = json_decode($e->getResponseBody());
        }

        return new JsonResponse($response);
    }

    /**
     * Returns an array with scope and description key-value pairs
     *
     * @param ConsentRequest $request
     * @return array
     */
    private function getRequestedScopes(ConsentRequest $request)
    {
        $scopes = [];
        $requestedScopes = $request->getRequestedScope();
        foreach ($requestedScopes as $requestedScope) {
            $scopes[$requestedScope] = $this->scopeProvider->getScopeDescription($requestedScope);
        }

        return $scopes;
    }

    /**
     * Returns an object of custom user claims
     *
     * @param string $userId
     * @return object
     */
    private function getUserClaims(string $userId)
    {
        return (object) [
            "usr" => $this->userClaimsProvider->getUserClaims($userId)
        ];
    }

    /**
     * Returns the CSRF Guard
     *
     * @param ServerRequestInterface $request
     * @return CsrfGuardInterface
     */
    private function getCSRFGuard(ServerRequestInterface $request): CsrfGuardInterface
    {
        return $request->getAttribute(CsrfMiddleware::GUARD_ATTRIBUTE);
    }
}
