<?php


namespace Vallarj\Mezzio\HydraClient\Handler;


use Vallarj\Mezzio\HydraClient\Provider\ScopeProvider;
use Vallarj\OAuth2\Client\Provider\Hydra;
use Laminas\Diactoros\Response\JsonResponse;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Ory\Hydra\Client\Api\AdminApi;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SessionHandler implements RequestHandlerInterface
{
    /** @var Hydra */
    private $hydraClientProvider;

    /** @var AdminApi */
    private $hydraAdmin;

    /** @var ScopeProvider */
    private $scopeProvider;

    /**
     * SessionHandler constructor.
     *
     * @param Hydra $hydraClientProvider
     * @param AdminApi $hydraAdmin
     * @param ScopeProvider $scopeProvider
     */
    public function __construct(
        Hydra $hydraClientProvider,
        AdminApi $hydraAdmin,
        ScopeProvider $scopeProvider
    ) {
        $this->hydraClientProvider = $hydraClientProvider;
        $this->hydraAdmin = $hydraAdmin;
        $this->scopeProvider = $scopeProvider;
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $session = $this->getSession($request);

        if ($session->has("user_id")) {
            // Check validity of stored access token
            try {
                $result = $this->hydraAdmin->introspectOAuth2Token($session->get("access_token"));
                $refresh = !$result->getActive();
            } catch (\Exception $e) {
                // Token is invalid or expired
                $refresh = true;
            }

            $expired = false;
            if ($refresh) {
                try {
                    $token = $this->hydraClientProvider->getAccessToken('refresh_token', [
                        'refresh_token' => $session->get("refresh_token"),
                        'nonce' => $session->get("oauth2nonce")
                    ]);

                    $session->set("access_token", $token->getToken());
                    $session->set("refresh_token", $token->getRefreshToken());
                    $session->set("expires", $token->getExpires());
                } catch (IdentityProviderException $e) {
                    $expired = true;
                }
            }

            if (!$expired) {
                $response = (object)[
                    "logged_in" => true,
                    "user_id" => $session->get("user_id"),
                    "usr" => $session->get("usr"),
                    "access_token" => $session->get("access_token"),
                    "expires" => $session->get("expires")
                ];

                return new JsonResponse($response, 200, [], JSON_PRETTY_PRINT);
            }
        }

        // Clear the session
        $session->clear();

        // Fetch authorization URL
        $authorizationUrl = $this->hydraClientProvider->getAuthorizationUrl([
            "scope" => $this->scopeProvider->getScopes()
        ]);

        // Set state and nonce
        $session->set('oauth2state', $this->hydraClientProvider->getState());
        $session->set('oauth2nonce', $this->hydraClientProvider->getNonce());

        $response = (object)[
            "logged_in" => false,
            "redirect_to" => $authorizationUrl
        ];

        return new JsonResponse($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Returns the session container
     *
     * @param ServerRequestInterface $request
     * @return SessionInterface
     */
    public function getSession(ServerRequestInterface $request): SessionInterface
    {
        return $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);
    }
}
